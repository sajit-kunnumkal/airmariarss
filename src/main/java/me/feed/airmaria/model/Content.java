package me.feed.airmaria.model;

/**
 * Created by sajit.kunnumkal on 11/30/2016.
 */
public class Content {

    private String url;
    private String title;

    private String date;

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }


    private String description;

    public String getDate() {
        return date;
    }

    public Content(String url, String title, String date){
        this(url,title,date,"Ave Maria!");
    }

    public Content(String url,String title, String date, String description) {
        this.url = url;
        this.title = title;
        this.date = date;
        this.description = description;

    }

    @Override
    public String toString(){
        return "{title:"+title+",url:"+url+",date:"+date+"}";
    }

    public String getDescription() {
        return description;
    }


}
