package me.feed.airmaria.rssutils;

import com.sun.syndication.feed.synd.*;
import com.sun.syndication.io.SyndFeedOutput;
import me.feed.airmaria.model.Feed;
import me.feed.airmaria.model.FeedMessage;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RSSFeedWriterV2 implements RSSFeedWriter {


    @Override
    public String write(Feed rssFeed) throws Exception {
        SyndFeed feed = new SyndFeedImpl();
        feed.setFeedType("rss_2.0");
        feed.setTitle(rssFeed.getTitle());
        feed.setDescription(rssFeed.getDescription());
        feed.setAuthor("sajit.kunnumkal@gmail.com");
        feed.setLanguage(rssFeed.getLanguage());
        feed.setLink(rssFeed.getLink());
        feed.setPublishedDate(new Date());
        List entries = new ArrayList();
        for(FeedMessage feedMessage : rssFeed.getMessages()) {
            SyndEntry entry = new SyndEntryImpl();
            SyndContent description;

            entry.setTitle(feedMessage.getTitle());
            entry.setLink(feedMessage.getLink());
            entry.setAuthor("AirMaria");
            LocalDate date = LocalDate.parse(feedMessage.getDate(), DateTimeFormatter.ofPattern("yyyy/MM/dd"));
            entry.setPublishedDate(Date.from(Instant.from(date.atStartOfDay(ZoneId.of("America/New_York")))));
            description = new SyndContentImpl();
            description.setType("text/html");
            description.setValue(feedMessage.getDescription());
            entry.setDescription(description);
            entries.add(entry);
        }
        feed.setEntries(entries);


        return new SyndFeedOutput().outputString(feed);

    }
}
