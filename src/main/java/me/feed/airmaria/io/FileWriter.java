package me.feed.airmaria.io;

public interface FileWriter {

    void writeFile(String content);
}
