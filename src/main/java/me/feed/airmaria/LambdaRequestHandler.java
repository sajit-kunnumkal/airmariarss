package me.feed.airmaria;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import me.feed.airmaria.io.FileWriter;
import me.feed.airmaria.io.S3FileWriter;
import me.feed.airmaria.model.Content;
import me.feed.airmaria.model.Feed;
import me.feed.airmaria.rssutils.RSSFeedWriter;
import me.feed.airmaria.rssutils.RSSFeedWriterV2;
import me.feed.airmaria.rssutils.TranslateUtil;

public class LambdaRequestHandler implements RequestHandler<Map<String,String>,String> {

    private static final Logger logger = LoggerFactory.getLogger(LambdaRequestHandler.class);


    @Override
    public String handleRequest(Map<String,String> s, Context context) {
        Integer numDays = Integer.valueOf(s.getOrDefault("numDays","2"));
        logger.info("Checking feed for {} days",numDays);
        LinkExtractor le = new LinkExtractor(numDays.intValue());
        List<Content> contents = le.getContent(LocalDate.now());
        for(Content content : contents){
            logger.info(content.toString());
        }

        Feed feed = TranslateUtil.convert(contents);
        RSSFeedWriter writer = new RSSFeedWriterV2();

        try {
            String rssFeedString = writer.write(feed);
            logger.info("Finished creating feed");
            FileWriter fileWriter = new S3FileWriter();
            fileWriter.writeFile(rssFeedString);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            System.exit(1);

        }

        return "created file for "+ LocalDate.now();
    }


}
