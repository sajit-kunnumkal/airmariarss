package me.feed.airmaria.rssutils;

import me.feed.airmaria.model.Feed;

public interface RSSFeedWriter {

    String write(Feed rssFeed) throws Exception;
}
