package me.feed.airmaria.io;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.awscore.exception.AwsServiceException;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.ObjectCannedACL;
import software.amazon.awssdk.services.s3.model.PutObjectAclRequest;
import software.amazon.awssdk.services.s3.model.PutObjectAclResponse;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;

public class S3FileWriter implements FileWriter{

    private static final Logger logger = LoggerFactory.getLogger(S3FileWriter.class);
    // Download the image from S3 into a stream
    S3Client s3Client = S3Client.builder().build();

    @Override
    public void writeFile(String content) {
        putObject(s3Client,content,"airmariafeeds","rss.feed");

    }

    private void putObject(S3Client s3Client,String content,
                           String bucket, String key) {



        PutObjectRequest putObjectRequest = PutObjectRequest.builder()
                .bucket(bucket)
                .key(key)
                .build();

        PutObjectAclRequest putObjectAclRequest = PutObjectAclRequest.builder()
                .bucket(bucket)
                        .key(key).acl(ObjectCannedACL.PUBLIC_READ).build();

        // Uploading to S3 destination bucket
        logger.info("Writing to: " + bucket + "/" + key);
        try {
            PutObjectResponse putObjectResponse = s3Client.putObject(putObjectRequest,
                    RequestBody.fromBytes(content.getBytes()));
            logger.info("Uploaded object {}",putObjectResponse.requestChargedAsString());

            PutObjectAclResponse putObjectAclResponse = s3Client.putObjectAcl(putObjectAclRequest);
            logger.info("Object ACL {}",putObjectAclResponse.sdkHttpResponse().statusCode());
        }
        catch(AwsServiceException e)
        {
            logger.error(e.awsErrorDetails().errorMessage());
            System.exit(1);
        }
    }
}
