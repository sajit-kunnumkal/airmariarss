package me.feed.airmaria.model;

public class FeedCreateException extends Exception {

    public FeedCreateException(Throwable t) {
        super(t);
    }

}
