package me.feed.airmaria;

import me.feed.airmaria.model.Content;
import me.feed.airmaria.model.Feed;
import me.feed.airmaria.rssutils.RSSFeedWriter;
import me.feed.airmaria.rssutils.RSSFeedWriterV2;
import me.feed.airmaria.rssutils.TranslateUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class LinkExtractor {

    private static Logger logger = LoggerFactory.getLogger(LinkExtractor.class);

    private final int numDays;
    private static String BASE_URL = "https://airmaria.com/";
    public LinkExtractor(int numDays) {
        this.numDays = numDays;
    }

    public List<Content> extract(String url,String dateStr)  {
        logger.info("Calling url {}",url);
        //main url for the day
        Document doc = null;
        try {
            doc = Jsoup.connect(url).get();
            Elements links = doc.select("a");

            Iterable<Element> iterable = () -> links.iterator();
            Stream<Element> targetStream = StreamSupport.stream(iterable.spliterator(), false);
            //Get unique urls with links airmaria.com/yyyy/mm/dd
            Stream<String> mp3WebpageLinks = targetStream.filter(element ->
                    element.attr("href").startsWith(url) && !element.attr("href").contains("#")).
                    map(element -> element.attr("href")).
                    distinct();

            return getContent(mp3WebpageLinks,dateStr).collect(Collectors.toList());
        } catch (IOException e) {
            logger.error(e.getMessage());
            return new ArrayList<>();
        }



    }

    private Stream<Content> getContent(Stream<String> mp3WebpageLinks, String dateStr) {

        return mp3WebpageLinks.map(pageLink -> {
            try {
                Document doc = Jsoup.connect(pageLink).get();


                Iterable<Element> links = () -> doc.select("a").iterator();
                Stream<Element> targetStream = StreamSupport.stream(links.spliterator(), false);
                Optional<String> mp3url = targetStream
                        .map(el -> el.attr("href"))
                        .filter(urlStr -> urlStr.contains(".mp3"))
                        .map(url -> url.trim())
                        .distinct().findFirst();
                if(mp3url.isPresent()){
                    String title = doc.select("h1.entry-title").text();
                    String description = "Ave Maria";
                    return new Content(mp3url.get(),title,dateStr,description);

                }
                else {
                    logger.warn("No mp3 url found in {}",pageLink);
                }


            } catch (IOException e) {
                logger.error(e.getMessage());
            }
            return null;

        }).filter(Objects::nonNull);

    }

    public static void main(String[] args) throws Exception {
        long startTime = System.currentTimeMillis();

        LinkExtractor le = new LinkExtractor(1);
        List<Content> contents = le.getContent(LocalDate.now());
        for(Content content : contents){
            logger.info(content.toString());
        }

        Feed feed = TranslateUtil.convert(contents);
        RSSFeedWriter writer = new RSSFeedWriterV2();
        logger.info(writer.write(feed));
        System.out.println("Execution time in millis" + (System.currentTimeMillis()-startTime));

    }

    public List<Content> getContent(LocalDate startDate)  {
        List<Content> all = new ArrayList<>();

        for(int i=0;i<numDays;i++){
            LocalDate date = startDate.minusDays(i);
            String formattedDate = date.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
            all.addAll(extract(BASE_URL+formattedDate+"/",formattedDate));
        }
        return all;
    }


}
