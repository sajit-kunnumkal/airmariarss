package me.feed.airmaria.rssutils;

import me.feed.airmaria.model.Content;
import me.feed.airmaria.model.Feed;
import me.feed.airmaria.model.FeedMessage;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TranslateUtil {


    public static Feed convert(final List<Content> contents) {


        List<FeedMessage> messages = contents.stream().map(content -> {
            FeedMessage message = new FeedMessage();
            message.setAuthor("airmaria");
            message.setTitle(content.getTitle());
            message.setLink(content.getUrl());
            message.setDescription(content.getDescription());
            message.setGuid(UUID.randomUUID().toString());
            message.setDate(content.getDate());
            return message;
        }).collect(Collectors.toList());
        return new Feed("AirMaria RSS Feed","http://airmaria.com/","A feed for airmaria.com, Praise be to God","en-us","",new Date().toString(),messages);


    }
}
